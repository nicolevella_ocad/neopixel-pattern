/*

  Project:      NeoPixel Assignment
  Student:      Nicole Vella
  Course:       DGIF 2002 - Physical Computing - OCAD University
  Created on:   November 19, 2019
  Based on:     Week 10 Lesson, Lindy Wilkins DGIF 2002 Class
                Strandtest example code from Adafruit_NeoPixel.h library

*/


#include <Adafruit_NeoPixel.h> // include the neopixel library
#ifdef __AVR__
#include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

#define LED_PIN   6 // pin connected to neopixel

#define LED_COUNT 8 // total LEDs on strip

Adafruit_NeoPixel strip(LED_COUNT, LED_PIN); // declare the pixel strip

void setup() {
  strip.begin();           // init pixel strip
  strip.show();            // turn off all pixels
  strip.setBrightness(50); // set brightness
}

void loop() {

  prideFlagCycle(100); // a function that cycles through the colors of the pride flag, takes one argument which controls the speed of the color cycle

}



// a function that cycles through the colors of the pride flag, takes one argument which controls the speed of the color cycle
void prideFlagCycle(int pause) {

  // cycle through all pixels in the strip, setting each to red
  for (int i = 0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, strip.Color(255, 0, 0)); // set RGB pixel value
    strip.show();                                   // update the strip with RGB value
    delay(pause);                                   // pause for a bit before moving to the next pixel
  }

  // cycle through all pixels in the strip, setting each to orange
  for (int i = 0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, strip.Color(255, 90, 0));
    strip.show();
    delay(pause);
  }

  // cycle through all pixels in the strip, setting each to yellow
  for (int i = 0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, strip.Color(255, 255, 0));
    strip.show();
    delay(pause);
  }

  // cycle through all pixels in the strip, setting each to green
  for (int i = 0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, strip.Color(0, 255, 0));
    strip.show();
    delay(pause);
  }

  // cycle through all pixels in the strip, setting each to blue
  for (int i = 0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, strip.Color(0, 0, 255));
    strip.show();
    delay(pause);
  }

  // cycle through all pixels in the strip, setting each to purple
  for (int i = 0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, strip.Color(128, 0, 255));
    strip.show();
    delay(pause);
  }

}